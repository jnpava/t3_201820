package model.data_structures;

public class IQueue<T>{
	/**
	 * Relaci�n con la lista de la que se basa esta estructura
	 */
	private Lista<T> listica;
	
	private int tamanio;
	
	public  IQueue(){
		listica=new Lista<T>();
		tamanio=0;
	}
	/**
	 * Indica si la cola est� o no vac�a
	 */
	public boolean isEmpty(){
		if(tamanio==0){
			return true;
		}
		return false;
	}
	/**
	 * Retorna el tama�o de la cola
	 */
	public int size(){
		return tamanio;
	}
	
	/**
	 * Agrega un elemento al final de la cola
	 */
	public void enColar(T item){
		listica.agregarElemFinal(item);

		tamanio++;
	}
	
	/**
	 * Extrae el primer elemento de la cola
	 */
	public T desEncolar(){
		T item=listica.darPrimero().darItem();
		listica.primero=listica.primero.darSiguiente();
		if(isEmpty()) listica.ultimo=null;
		tamanio--;
		return item;
	}

}