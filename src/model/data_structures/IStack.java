package model.data_structures;

public class IStack<T> {
	
	
private Lista<T> listica;
	
	private int tamano;
	
	public IStack(){
		listica=new Lista<T>();
		tamano=0;
	}
	/**
	 * Retorna true si la Pila esta vacia
	 * @return true si la Pila esta vacia, false de lo contrario
	 */
	public boolean isEmpty(){
	if (tamano==0){
		return true;
	}
	return false;
	}
	/**
	 * Retorna el numero de elementos contenidos
	 * @return el numero de elemntos contenidos
	 */
	public int size(){
		return tamano;
	}
	
	/**
	 * Inserta un nuevo elemento en la Pila
	 * @param t el nuevo elemento que se va ha agregar
	 */
	public void push(T itemNuevo){
		Nodo<T> antiguoPrimero=listica.darPrimero();
		listica.primero=new Nodo<>();
		listica.primero.modificarActual(itemNuevo);
		listica.primero.cambiarSiguiente(antiguoPrimero);
		tamano++;
	}
	
	/**
	 * Quita y retorna el elemento agregado más recientemente
	 * @return el elemento agregado más recientemente
	 */
	public T pop() {
		T item=listica.darPrimero().darItem();
		listica.primero=listica.primero.darSiguiente();
		tamano=tamano-1;
		return item;
	}
	
}
