package model.data_structures;

import java.util.Iterator;





public class Lista<T>implements IDoublyLinkedList<T> {
	
	/* 
	 * nodo primero
	 */
	public Nodo<T> primero;
	/* 
	 * nodo ultimo
	 */
	public Nodo<T> ultimo;
	/*
	 * tama�o lsita
	 */
	public int tamanio;
/*
 * nodo en la posicion actual
 */
public Nodo<T> actual;

public Lista() {
	primero=null;
	ultimo=primero;
	tamanio=0;
	actual=primero;
}
public Nodo<T>darPrimero(){
	return primero;
}

public Nodo<T> darUltimo(){
	return ultimo;
}
private class Iterador<T> implements Iterator<T>{
	private int indice;
	public  Iterador() {
		// TODO Auto-generated constructor stub
		indice=-1;
	}
	
	/**
	 * Valida si hay un siguiente nodo
	 */
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
	return indice+1<tamanio;
		
	}
	/**
	 * Retorna el item del nodo siguiente
	 */
	@Override
	public T next() {
		// TODO Auto-generated method stub
		if(hasNext()){
			indice++;
			return (T) darElemento(indice);
			}
		
		
		else return null;
		
	}
	
}
public Iterator<T> iterator() {
	// TODO Auto-generated method stub
	Iterador<T> iterador= new Iterador<T>();
	return iterador;
}

@Override
public void agregarElemFinal(T elem) {
	Nodo<T> actual1=primero;
	Nodo<T> nuevo= new Nodo<>();
	nuevo.modificarActual(elem);
	if(primero ==null){
		primero=nuevo;
		actual=primero;
		ultimo=primero;
		tamanio++;
		return;
	}
	
	while(actual1.darSiguiente()!=null){
		actual1=actual1.darSiguiente();
	}
	actual1.cambiarSiguiente(nuevo);
	ultimo=actual1.darSiguiente();
	actual1=actual1.darSiguiente();
	tamanio++;
	
}
@Override
public T darElemento(int pos) {
	// TODO Auto-generated method stub
			Nodo<T> actual1=primero;
			if(primero==null||pos>tamanio-1){
				return null;
			}
			for(int i=-1;i<pos-1&&actual!=null;i++){
				actual1=actual1.darSiguiente();
			}
			
			return actual1.darItem();
		}

@Override
public int darNumeroElementos() {
	// TODO Auto-generated method stub
	return tamanio;
}
@Override
public T darElementoPosicionActual() {
	if(actual==null){
		return null;
	}
	return actual.darItem();
}
@Override
public boolean avanzarSiguientePosicion() {
	// TODO Auto-generated method stub
	if(actual==null||actual.darSiguiente()==null){
		return false;
	}
	actual=actual.darSiguiente();
	return true;
}
@Override
public boolean retrocederPosicionAnterior() {
	// TODO Auto-generated method stub
			if(actual==null||actual.equals(primero)){
				return false;
			}
			Nodo<T> actual1=primero;
			Iterador<T> iterador=(Iterador<T>) iterator();
			iterador.next();
			while(!iterador.next().equals(actual.darItem())){
				
				actual1=actual1.darSiguiente();
			}
			actual=actual1;
			
			return true;
}
}
